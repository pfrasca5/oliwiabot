#!/usr/bin/env python
import json
import logging
import random
import re
from random import randint
from typing import List

import requests
import wikitextparser as wtp
from pyrogram import filters
from pyrogram.client import Client
from pyrogram.errors import WebpageCurlFailed
from simpleeval import simple_eval
from wikitextparser import remove_markup
#from pyrogram.types import InputMediaPhoto

logging.basicConfig(filename='log.txt', level=logging.WARNING)
log = logging.getLogger(__name__)

with open("ids.json", "r") as f:
	data = json.loads(f.read())
	oliwia = data['id']['oliwia']
	palmas = data['id']['palmas']
	o_k_dm = data['chat_id']['oliwia-kris-dm']
	o_k_gr = data['chat_id']['oliwia-kris-gr']
	gelbooru_user = data['api_keys']['gelbooru_user']
	gelbooru_key = data['api_keys']['gelbooru_key']

#@Client.on_raw_update()
#def parse_raw_update(client, update, users, chats, message):
	#if type(update).__name__ == "UpdateMessageReactions":
		#last_emoji = update.reactions.results[-1].reaction.emoticon
		#if hasattr(update.peer, 'channel_id'):
			#peer_id = int("-100" + str(update.peer.channel_id))
		#elif hasattr(update.peer, 'chat_id'):
			#peer_id = int("-" + str(update.peer.chat_id))
		#else:
			#peer_id = update.peer.user_id
		#message_id = update.msg_id
		#message.text_reply("message_id: " + str(message_id))


no_gen = [
	'-fusion', '-colored_sclera', '-parody', 
	'-ugandan_knuckles', '-alternate_form',
	'-alternate_species', '-jigglypuff',
	'-possession', '-nightmare_fuel', 
	'-doll', '-crossover', '-cursed_image',
	'-hasbro', '-black_sclera', '-nazi', '-disney', '-kirby_(series)'
]
no_cgi = ['-3d', '-cgi']
ew_artists = [
	'-higgyy', '-mykegreywolf', '-cusith-baby',
	'-pubs_(artist)', '-darkfang100', '-floof-tigress', '-misaginus',
	'-moorsheadfalling', '-aakashi', '-nikraccoom', '-ry-spirit',
	'-unknown_artist', '-kenket', '-hale', '-karate_akabon', '-argos90',
	'-lewdwigvondrake',
]
no_furry = [
	'-canid', '-mouse','-aggressive_retsuko',
	'-demon', '-dragon', '-protogen']
no_boobers = [
	'-huge_breasts', '-huge_thighs', '-thick_thighs', 
	'-big_breasts', '-voluptuous', '-big_butt', '-butt',
	'-curvy_figure', '-breasts', '-bikini', '-skimpy',
	'-kissing', '-seductive', '-wide_hips', '-<3',
	'-licking', '-tits'
]   
kris_exc = [
	'-bridal_carry', '-hand_holding', '-romantic', 
	'-vocaloid', '-amouge','-bride', '-clothing_swap'
	]
no_fetish = [
	'-hyper', '-crossgender', '-muscular',
	'-anthrofied', '-transformation', '-humanization',
	'-ugly_man', '-ugly_bastard', '-body_writing',
	'-rape', '-yaoi', '-multiple_balls', '-interspecies',
	'-interracial', '-horse_cock', '-animal_sex',
	'-peeing', '-pee', '-pee_stain', '-child', '-baby', 
	'-tentacle_sex', '-parasite', '-tentacles',
	'-bug', '-beastiality', '-bondage',
	'-animal_penis', '-knotted_penis', '-monster',
	'-size_difference', '-giant', '-giantess',
	'-gigantic_breasts', '-huge_nipples',
	'-long_nipples', '-inflation', '-weight_gain', 
	'-slightly_chubby', '-gore', '-scat', '-pregnant', 
	'-inflation', '-fat', '-diaper', '-feet',
	'-foot_focus', '-dark_skin', '-foot_fetish',
	'-cub', '-overweight', '-forced', '-leash',
	'-loli', '-bdsm', '-disembodied_penis', '-urine', 
	'-chastity_cage'
]
no_fetish_van = ['-hyper', '-crossgender',
	'-anthrofied', '-*ation',
	'-body_writing', '-yaoi', '-interracial',
	'-*pee*', '-bug', '-bondage',
	'-gigantic_breasts', '-*nipples',
	'-weight_gain', '-slightly_chubby', 
    '-gore', '-scat', '-pregnant', 
    '-fat', '-diaper', '-feet',
	'-foot_focus', '-dark_skin', '-foot_fetish',
	'-cub', '-overweight', '-forced', '-leash',
	'-urine'
]
no_males = [
	'-1boy', '-2boys', '-multiple_boys', '-hetero',
	'-faceless_male', '-dark_skinned_male', '-sonic_the_hedgehog',
	'-miles_prower', '-knuckles_the_echidna', '-damian_desmond',
	'-male_focus', '-male', '-bald', '-midoriya_izuku', '-male/male',
	'-bowser', '-mario', '-groom'
]

schifo_w_m = no_cgi + no_fetish + no_gen
schifo = no_cgi + no_males + no_fetish + no_gen
kris_nonos = no_gen + no_furry + no_boobers + no_fetish_van + kris_exc

no_video = ['-video', '-animation']

g_website = "https://gelbooru.com/index.php?"
g_key = "api_key=" + gelbooru_key + "&user_id=" + str(gelbooru_user) + "&"
g_params = "page=dapi&s=post&json=1&q=index&limit=1&tags=sort:random+"
gelbooru_api = g_website + g_key + g_params

e_website = "https://e621.net/posts.json?"
e_params = "limit=1&tags=order:random+"
e621_api = e_website + e_params


base_tags = []
tags = []
should_have_m = False
consfw = True
leniency = 0
isKris = False
isSame = True

@Client.on_message(filters.command("report|r", "!"))
def report(client, message):
	(link, error_stat) = diocanajadedio(message)
	if link != "":
		isKris = check_kris(message)
		isSame = checkino_link(link, isKris)
		if not isSame:
			blacklist = open('blacklist.txt', 'a+')
			blacklist_k = open('blacklist_k.txt', 'a+')
			if isKris:
				blacklist_k.write(link + '\n')
				blacklist_k.close()
			else:
				blacklist.write(link + '\n')
				blacklist_k.write(link + '\n')
				blacklist.close()
				blacklist_k.close()
			message.reply_text('Link aggiunto con successo alla lista')
		else:
			message.reply_text('Link già presente nella lista')
		message.reply_to_message.delete()
	else:
		message.reply_text('Error:\n' + '\n'.join(error_stat))

@Client.on_message(filters.command("verifica|v", "!"))
def porcodidiooooo(client, message):
	(link, error_stat) = diocanajadedio(message)
	if link != "":
		isKris = check_kris(message)
		isSame = checkino_link(link, isKris)
		if isSame:
			message.reply_text("presente")
		else: 
			message.reply_text("non presente")
	else:
		message.reply_text('Error:\n' + error_stat)
	

@Client.on_message(filters.command("test", "!"))
def test(client, message):
	#message.reply_text("<code>icoli</code>", quote=True)
	message.reply_text("icoli", quote=True)


@Client.on_message(filters.command("mlaatr", "!"))
def mlaatr(client, message):
	message.reply_text("https://archive.org/details/teenage-robot-full-series")

set_img = """•oliwia (arg) (nsfw) (alt stella)\n•emily (arg) (nsfw) (alt emilia)\n•aurora (arg) (nsfw) (alt ro)\n•rosalinda (nsfw) (alt rosalina)
•jenny (nsfw)\n•anna (nsfw)\n•emma (arg)\n•kamina (arg) (nsfw)\n•eva (nsfw)\n•michiru (nsfw)\n•nazuna (nsfw)\n•michizuna (nsfw)\n•miao\n•femboy\n•sofia\n•basso
•kris\n•niko\n•ralsei\n•noelle\n•temmie\n•mizore\n•blessed\n•tails\n•amy\n•sticks\n•kooty\n•nickit\n•char (arg)\n•sybil\n"""
set_func = "•test\n•report\n•horny\n•status\n•mlaatr\n•sesso (str)\n•calc (expr)\n•cerca\n•ext\n•fix (broken)\n"
set_div = "•poppe\n•sto\n•no\n•misura\n•misure\n•nome\n•porco"

listaoli = 'random, ochako, jenny, tsuyu, futaba, daisy, blank, kumiko, michiru, gloria, vera, tsubasa, stella, pyra, noelle, kris, temmie, hajime, miyako, haruhi, isabelle, amy, yui, anya, mela, mirai, teto, nikki, velma, ankha, sage, 5volt, purah, yukari, kurumi, neco, arcueid, pomni, shiki, toko, kuromi, outis, sadayo, ryuko'
listaemi = 'miku, marnie, shygal, raven, nazuna, blaze, hutao, patchouli, rei, lucy, rebecca, muffet, komi, mei, vivian, ragatha, bridget'
listaro = 'mizore, rosalinda, kikuri, rinko, pa-san'
listaemma = 'kangel, frye, osaka'
listakamina = 'domon, may, char, modeus, ram, suletta'

@Client.on_message(filters.command("aiuto", "!"))
def aide(client, message):
	checkino = " ".join(message.text.split()[1:])
	bucchin = [
	'Comandi disponibili\n',
	"Mandano immagini (non aggiungere le parentesi)\n(possibile spoilerare le immagini aggiungendo spl, puoi specificare il numero di immagini che vuoi mandare fino a un massimo di 10)\n",
	'\nFunzionalità base\n', '\nStupide cose\n'
	]
	if checkino != "":
		match checkino:
			case 'oliwia': comandi = "Manda disegni a immagine e somiglianza di Oliwia\nArgomenti disponibili: " + listaoli
			case 'emily': comandi = "Manda disegni a immagine e somiglianza di Emily\nArgomenti disponibili: " + listaemi
			case 'emilia': comandi = "Manda disegni a immagine e somiglianza di Emily\nArgomenti disponibili: " + listaemi
			case 'aurora': comandi = "Manda disegni a immagine e somiglianza di Aurora\nArgomenti disponibili: " + listaro
			case 'kamina': comandi = "Manda disegni di pg che piacciono a Kamina\nArgomenti disponibili: " + listakamina
			case 'anna': comandi = "Manda disegni a immagine e somiglianza di Anna"
			case 'emma': comandi = "Manda disegni a immagine e somiglianza di Emma\nArgomenti disponibili: " + listaemma
			case 'eva': comandi = "Manda disegni a immagine e somiglianza di Eva"
			case 'rosalinda': comandi = "Manda immagini con Rosalinda"
			case 'rosalina': comandi = "Manda immagini con Rosalinda"
			case 'jenny': comandi = "Manda immagini con Jenny"
			case 'miao': comandi = "Manda nekogirl"
			case 'femboy': comandi = "Manda femboy"
			case 'sofia': comandi = "Manda immagini feticiste"
			case 'basso': comandi = "Manda ragazze che hanno un basso"
			case 'kris': comandi = "Manda immagini con Kris"
			case 'noelle': comandi = "Manda immagini con Niko"
			case 'blessed': comandi = "(Almeno prova) Manda immagini wholesome"
			case 'ralsei': comandi = "Manda immagini con Ralsei"
			case 'noelle': comandi = "Manda immagini con Noelle"
			case 'mizore': comandi = "Manda immagini con Mizore"
			case 'amy': comandi = "Manda immagini con Amy"
			case 'sticks': comandi = "Manda immagini con Sticks"
			case 'kooty': comandi = "Manda immagini con Klonoa"
			case 'temmie': comandi = "Manda immagini con Temmie"
			case 'sybil': comandi = "Manda immagini con Sybil"
			case 'test': comandi = "Comando di test per vedere se il bot è attivo"
			case 'horny': comandi = "Attiva o disattiva il filtro per le immagini nsfw\nSolo Oliwia può usarlo"
			case 'status': comandi = "Verifica se il filtro è attivo o meno"
			case 'mlaatr': comandi = "Manda il link per guardare __My Life As A Teenage Robot__"
			case 'sesso': comandi = "Calcola il tuo valore di sessaggine\n!sesso -> Sei sesso al ...% I !sesso qualcosa -> Qualcosa è sesso al ...%\nPotrebbe dare risultati diversi con alcuni valori"
			case 'calc': comandi = "Calcolatrice basilare\n!calc 1+1 -> 2"
			case 'misura': comandi = "Calcola la lunghezza della tua minchia"
			case 'misure': comandi = "Calcola la grandezza delle tue zizze"
			case 'sto': comandi = "Risponde con cazzo"
			case 'report': comandi = "Mette il link dell'immagine scelta in una blacklist, per evitarne future apparizioni"
			case 'no': comandi = "Risponde con una parola a caso aggiunta a no\n!no -> no bitches?"
			case 'nome': comandi = "Dà un nome proprio a caso"
			case 'porco': comandi = "Genera una bestemmia nel formato\nporco : nome : participio passato di un verbo (a volte non presente)"
			case 'cerca': comandi = "Cerca uno sticker pack"			
			case 'char': comandi = """Manda immagini dalla serie Little Tail Bronx\n!char x y\nx indica il gioco da cui viene ([s] Solatorobo, [t] Tail Concerto, [f] Fuga)\nPersonaggi disponibili:\ns (red, chocolat, elh, beluga, quebec, flo, bruno, opera, gren, calua, merveille, nero, blanck, carmine, rose)\nt (waffle, alicia, flare, stare, panta, russel, terria, cyan, fool)\nf (malt, mei, hanna, boron, kyle, socks, chick, hack, sheena, jin, wappa, britz, muscat, shvein, flam, doktor blutwurst)"""
			case 'ext': comandi = "Estrae l'embed da un link Pinterest"
			case 'fix': comandi = "Manda versione fixata di un link Twitter/Instagram"
			case _: comandi = "Non è un comando"
	else:
		comandi = bucchin[0] + "\n" + bucchin[1] + set_img + bucchin[2] + set_func + bucchin[3] + set_div
	#message.reply_text("<code>" + comandi + "</code>")
	message.reply_text(comandi)

@Client.on_message(filters.command("help", "!"))
def pomocy(client, message):
	checkino = " ".join(message.text.split()[1:])
	bucchin = ['Available commands\n', "Image senders (don't add parenthesis)\nCan flag as spoiler by adding spl right after the command and specify the amount of images to send, up to 10\n",'\nBasic functions\n','\nStupid things\n']
	if checkino != "":
		match checkino:
			case 'oliwia': comandi = "Sends images based on Oliwia's likeness\nAvailable arguments: " + listaoli
			case 'emily': comandi = "Sends images based on Emily's likeness\nAvailable arguments: " + listaemi
			case 'emilia': comandi = "Sends images based on Emily's likeness\nAvailable arguments: " + listaemi
			case 'aurora': comandi = "Sends images based on Aurora's likeness\nAvailable arguments: " + listaro
			case 'kamina': comandi = "Sends images with characters Kamina likes\nAvailable arguments: " + listakamina
			case 'anna': comandi = "Sends images based on Anna's likeness"
			case 'emma': comandi = "Sends images based on Emma's likeness\nAvailable arguments: " + listaemma
			case 'eva': comandi = "Sends images based on Eva's likeness"
			case 'rosalinda': comandi = "Sends images with Rosalina"
			case 'rosalina': comandi = "Sends images with Rosalina"
			case 'jenny': comandi = "Sends Jenny pics"
			case 'miao': comandi = "Sends nekogirls"
			case 'femboy': comandi = "Sends femboys"
			case 'sofia': comandi = "Sends foot fetish pics"
			case 'basso': comandi = "Sends bassist girls pics"
			case 'kris': comandi = "Sends Kris pics"
			case 'niko': comandi = "Sends Niko pics"
			case 'blessed': comandi = "Tries to send wholesome pics"
			case 'ralsei': comandi = "Sends Ralsei pics"
			case 'noelle': comandi = "Sends Noelle pics"
			case 'mizore': comandi = "Sends Mizore pics"
			case 'amy': comandi = "Sends Amy pics"
			case 'sticks': comandi = "Sends Sticks pics"
			case 'kooty': comandi = "Sends Klonoa pics"
			case 'temmie': comandi = "Sends Temmie pics"
			case 'sybil': comandi = "Sends Sybil pics"
			case 'test': comandi = "Test command to see if it works"
			case 'horny': comandi = "Turns explicit images on or off\nOnly Oliwia can use it"
			case 'status': comandi = "Checks whether the switch is turned on or off"
			case 'mlaatr': comandi = "Sends a link to watch My Life As A Teenage Robot"
			case 'sesso': comandi = "Gives your sexiness value, or that of a thing you added\n!sesso -> Sei sesso al ...% I !sesso qualcosa -> Qualcosa è sesso al ...%\nResult may vary depending on certain values"
			case 'calc': comandi = "Basic calculator\n!calc 1+1 -> 2"
			case 'misura': comandi = "Calculates your dick's size"
			case 'misure': comandi = "Calculates your tits' size"
			case 'sto': comandi = "Replies with cazzo"
			case 'report': comandi = "Adds the replied image link to a blacklist so it won't appear anymore"
			case 'no': comandi = "Replies with a random word after no\n!no -> no bitches?"
			case 'nome': comandi = "Gives a random given name"
			case 'porco': comandi = "Gives a random swear in this format\nporco : name : past participle of a verb (optional, sometimes won't show up)"
			case 'search': comandi = "Searches for a sticker pack"
			case 'char': comandi = """Sends pics from the Little Tail Bronx series\n!char x y\nx pointing out the game it comes from ([s] Solatorobo, [t] Tail Concerto, [f] Fuga)\nAvailable characters:\ns (red, chocolat, elh, beluga, quebec, flo, bruno, opera, gren, calua, merveille, nero, blanck, carmine, rose)\nt (waffle, alicia, flare, stare, panta, russel, terria, cyan, fool)\nf (malt, mei, hanna, boron, kyle, socks, chick, hack, sheena, jin, wappa, britz, muscat, shvein, flam, doktor blutwurst)"""
			case 'ext': comandi = "Extracts an embeddable pic from a Pinterest link"
			case 'fix': comandi = "Returns a fixed Twitter/Instagram link"
			case _: comandi = "This is not a command"
	else:
		comandi = bucchin[0] + "\n" + bucchin[1] + set_img + bucchin[2] + set_func + bucchin[3] + set_div
	#message.reply_text("<code>" + comandi + "</code>")
	message.reply_text(comandi)

@Client.on_message(filters.command("sesso", "!"))
def sessometro(client, message):
	nome = " ".join(message.text.split()[1:])
	ultrasesso = "emily|fabiola|emi|neek|sofia|yuri|fem[A-Za-z]$|donn.$|trans|mtf|jenny|rosalinda|mizore|aurora|ro'|nozomi|nicole|arianna|kooty|kootino|ilma|vittoria|florysta|mamacita"
	antisesso = "terf|omofob|transfob|yaoi|gore|femminicidio"
	if nome != "":
		match = re.search(ultrasesso, str(message.text), flags=re.IGNORECASE)
		match2 = re.search(antisesso, str(message.text), flags=re.IGNORECASE)
		napoli = re.search("oliwia|oliwieta", str(message.text), flags=re.IGNORECASE)
		if (match2 is not None or (match is not None and match2 is not None)):
			sessaggine = "Fa assolutamente schifo"
		elif match is not None and match2 is None:
			sessaggine = nome + " è ultrasesso"
		elif nome == "piedini":
			sessaggine = "sofia smettila"
		elif napoli is not None:
			sessaggine = nome + " è sesso di Napoli livello"
		else:
			sessaggine = nome + " è sesso al " + str(randint(0, 100)) + "%"
	else:
		sessaggine = "Sei sesso al " + str(randint(0, 100)) + "%"
	#message.reply_text("<code>" + sessaggine + "</code>")
	message.reply_text(sessaggine)

#inizio pic

@Client.on_message(filters.command("jenny", "!"))
def jenny(client, message):
	leniency = 1
	base_tags = ['jenny_wakeman'] + no_fetish_van
	tags[:] = []
	isKris = check_kris(message)
	if isKris:
		base_tags.append(kris_exc[3])
	pic2(client, message, base_tags, tags, leniency)
	
@Client.on_message(filters.command("oliwia|stella", "!"))
def oli(client, message):
	leniency = 1
	meg = " ".join(message.text.split()[1:])
	tag_set = [
		"1girl+brown_hair+glasses+short_hair", "uraraka_ochako",
		"jenny_wakeman", "asui_tsuyu", "sakura_futaba",
		"princess_daisy", "blank_(captainkirb)", "oumae_kumiko",
		"kagemori_michiru", "gloria_(pokemon)", "may_(pokemon)",
		"hanekawa_tsubasa", "starfire", "pyra_(xenoblade)",
		"noelle_holiday", "kris_(deltarune)", "temmie", "owari_hajime",
		"hoshino_miyako_(wataten)", "suzumiya_haruhi", "isabelle_(animal_crossing)",
		"amy_rose", "hirasawa_yui", "anya_(spy_x_family)+rating:general", "mela_(pokemon)",
		"kuriyama_mirai", "kasane_teto", "nikki_(swapnote)", "velma_dace_dinkley",
		"ankha_(animal_crossing)", "sage_(sonic)", "5-volt", "purah", "akiyama_yukari",
		"tokisaki_kurumi", "neco-arc", "arcueid_brunestud", "pomni_(the_amazing_digital_circus)",
		"misaki_shiki", "fukawa_toko", "kuromi+rating:general", "outis_(limbus_company)", "kawakami_sadayo",
		"matoi_ryuuko"
	]
	index = randint(0, len(tag_set)-1)
	if len(meg.split()) > 0:
		(index) = sceltina(str(meg.split()[0]), listaoli)
	base_tags = [tag_set[index]]
	tags[:] = no_fetish_van
	isKris = check_kris(message)
	if isKris: 
		tags.extend(kris_nonos)
		leniency = -1
	if index == 6 or index == 41:
		should_have_m = True
	else:
		should_have_m = False
	pic(client, message, base_tags, tags, leniency, should_have_m)

@Client.on_message(filters.command("emily|emilia", "!"))
def emily(client, message):
	leniency = 1
	meg = " ".join(message.text.split()[1:])
	tag_set = [
		"hatsune_miku", "marnie_(pokemon)", "shy_gal", "raven_(dc)",
		"hiwatashi_nazuna", "blaze_the_cat", "hu_tao_(genshin_impact)",
		"patchouli_knowledge", "ayanami_rei", "lucy_(cyberpunk)",
		"rebecca_(cyberpunk)", "muffet", "komi_shouko", "mei_(overwatch)",
		"vivian_(paper_mario)", "ragatha_(the_amazing_digital_circus)",
		"bridget_(guilty_gear)"
	]
	index = randint(0, len(tag_set))
	if len(meg.split()) > 0:
		(index) = sceltina(str(meg.split()[0]), listaemi)
	base_tags = [tag_set[index]] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("anna", "!"))
def annie(client, message):
	leniency = 2
	annina = ['marceline_abadeer', 'futanari']
	base_tags = [random.choice(annina)] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("aurora|ro", "!"))
def aurorina(client, message):
	leniency = 1
	meg = " ".join(message.text.split()[1:])
	tag_set = [
		'yoroizuka_mizore', 'rosalina', 'hiroi_kikuri', 
		'shirokane_rinko', 'pa-san'
	]
	index = randint(0, len(tag_set))
	if len(meg.split()) > 0:
		(index) = sceltina(str(meg.split()[0]), listaro)
	base_tags = [tag_set[index]] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("emma", "!"))
def emmina(client, message):
	leniency = -1
	meg = " ".join(message.text.split()[1:])
	match_set = listaemma.split(", ")
	tag_set = ['needy_girl_overdose', 'frye_(splatoon)', 'kasuga_ayumu']
	index = randint(0, len(match_set))
	if len(meg.split()) > 0:
		(index) = sceltina(str(meg.split()[0]), listaemma)
	base_tags = [tag_set[index]] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)	

@Client.on_message(filters.command("kamina", "!"))
def emmina(client, message):
	leniency = 0
	meg = " ".join(message.text.split()[1:])
	match_set = listakamina.split(", ")
	tag_set = ['domon_kasshu', 'may_(gundam_build_divers_re:rise)', 'char_aznable', 'modeus_(helltaker)', 'ram_(re:zero)', 'suletta_mercury']
	index = randint(0, len(match_set))
	if len(meg.split()) > 0:
		(index) = sceltina(str(meg.split()[0]), listakamina)
	base_tags = [tag_set[index]] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)	

@Client.on_message(filters.command("eva", "!"))
def eva(client, message):
	leniency = 1
	base_tags = ['katsuragi_misato'] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("noi", "!"))
def noi(client, message):
	leniency = 1
	base_tags = ['3girls'] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("neek", "!"))
def neek(client, message):
	leniency = 1
	base_tags = ['*long_hair', 'black_hair', '-pussy', '-bangs', 'flat_chest', 'solo']
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("miao", "!"))
def miao(client, message):
	leniency = 0
	base_tags = ['cat_girl'] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("mizore|mziore|mizorw|mizrow|mizroe|mziroe|mizoee|mixore|mizoew|mziuore|mziorew|mzireo|mzioire|miozre|mizoer", "!"))
def mamma(client, message):
	leniency = 0
	base_tags = ['yoroizuka_mizore'] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("rosalinda|rosalina", "!"))
def rosalinda(client, message):
	leniency = 0
	base_tags = ['rosalina', '-furry', '-super_smash_bros.', '-bowser', '-animal_crossing'] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("sofia|mno", "!"))
def sofia(client, message):
	leniency = 0
	base_tags = ['1girl', 'foot_focus'] + no_gen
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("michiru", "!"))
def michiru(client, message):
	leniency = 0
	base_tags = ['michiru_kagemori'] + no_fetish_van
	tags[:] = []
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("nazuna", "!"))
def nazuna(client, message):
	leniency = 0
	base_tags = ['nazuna_hiwatashi'] + no_fetish_van
	tags[:] = []
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("michizuna", "!"))
def michi(client, message):
	leniency = 0
	base_tags = ['nazuna_hiwatashi', 'michiru_kagemori'] + no_fetish_van
	tags[:] = []
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("bna", "!"))
def bna(client, message):
	eh = bool(random.getrandbits(1))
	if eh: michiru(client, message)
	else: nazuna(client, message)

@Client.on_message(filters.command("ralsei", "!"))
def ralsei(client, message):
	leniency = 0
	base_tags = ['ralsei', 'rating:safe'] + no_fetish_van
	tags[:] = no_boobers
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("kooty", "!"))
def kooty(client, message):
	base_tags = ['klonoa', 'rating:safe'] + no_fetish_van
	tags[:] = ['-savestate'] + no_fetish
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("nickit", "!"))
def nickit(client, message):
	leniency = 0
	base_tags = ['nickit'] + no_fetish_van
	tags[:] = no_fetish
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("temmie", "!"))
def temmie(client, message):
	leniency = 0
	base_tags = ['temmie_(undertale)'] + no_fetish_van
	tags[:] = []
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("noelle", "!"))
def noelle(client, message):
	leniency = 0
	base_tags = ['noelle_holiday'] + no_fetish_van
	tags[:] = []
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("niko", "!"))
def miaomiao(client, message):
	base_tags = ['niko_(oneshot)'] + no_fetish_van
	tags[:] = []
	ah = bool(random.getrandbits(1))
	leniency = -1
	if ah:
		pic(client, message, base_tags, tags, leniency, False)
	else: 
		pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("amy", "!"))
def cutie(client, message):
	leniency = 0
	base_tags = ['amy_rose'] + no_fetish_van
	tags[:] = []
	isKris = check_kris(message)
	if isKris: tags.extend(kris_nonos)
	else: tags.extend(no_gen)
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("sticks", "!"))
def bombastik(client, message):
	bombastus = bool(random.getrandbits(1))
	leniency = 0
	tags[:] = []
	tags.extend(no_gen)
	isKris = check_kris(message)
	if isKris:
		bombastus = False
	if bombastus:
		base_tags = ['sticks_the_badger'] + no_fetish_van
		pic(client, message, base_tags, tags, leniency, False)
	else: 
		base_tags = ['sticks_the_jungle_badger'] + no_fetish_van
		pic2(client, message, base_tags, tags, leniency)
		
@Client.on_message(filters.command("tails", "!"))
def foxboi(client, message):
	leniency = -1
	tags[:] = []
	tags = no_gen + no_fetish_van
	temp2 = []
	meg = " ".join(message.text.split()[1:])
	if meg=="fr":
		temp2.append('amy_rose')
	trick = bool(random.getrandbits(1))
	if trick:
		base_tags = ['miles_prower']
		base_tags.extend(temp2)
		pic2(client, message, base_tags, tags, leniency)
	else:
		base_tags = ['{tails_(sonic) ~ miles_prower}']
		base_tags.extend(temp2)
		pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("kris", "!"))
def kris(client, message):
	leniency = -1
	base_tags = ['kris_(deltarune)']
	base_tags.extend(no_fetish_van)
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("femboy", "!"))
def femboy(client, message):
	leniency = 0
	base_tags = ['1boy', 'thighhighs', '-1girl', '-2girls', 'trap', '-anal_object_insertion', '-pokemon', '-furry', '-tentacle_sex']
	tags[:] = []
	pic(client, message, base_tags, base_tags, leniency, True)

@Client.on_message(filters.command("ena", "!"))
def sha(client, message):
	leniency = 0
	base_tags = ['ena_(joel_g)'] + no_fetish_van
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("furry", "!"))
def woof(client, message):
	leniency = 0
	base_tags = ['furry']
	tags[:] = no_furry + no_fetish + no_gen
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("sybil", "!"))
def bomboclaat(client, message):
	leniency = 0
	base_tags = ['sybil_(pseudoregalia)']
	tags[:] = []
	pic2(client, message, base_tags, tags, leniency)

@Client.on_message(filters.command("blessed", "!"))
def wholesome(client, message):
	leniency = -1
	base_tags = no_fetish_van
	tags[:] = no_boobers + no_fetish
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("basso", "!"))
def jaco(client, message):
	leniency = 0
	base_tags = ['bass_guitar']
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

@Client.on_message(filters.command("blob", "!") & filters.user(palmas))
def palmino(client, message):
	leniency = 0
	base_tags = ['slime_(creature)']
	tags[:] = []
	pic(client, message, base_tags, tags, leniency, False)

def pdgmatch(base_tags, meg):
	char = ['waffle', 'alicia', 'flare', 'stare', 'panta',
	'russel', 'terria', 'cyan', 'fool', 'red',
	'chocolat', 'elh', 'beluga', 'quebec', 'flo',
	'bruno', 'opera', 'gren', 'calua', 'merveille',
	'nero', 'blanck', 'carmine', 'rose', 'malt',
	'mei', 'hanna', 'boron', 'kyle', 'socks',
	'chick', 'hack', 'sheena', 'jin', 'wappa',
	'britz', 'muscat', 'shvein', 'flam', 'doktor blutwurst']
	chartag = ['waffle_ryebread', 'alicia_pris', 'flare_pris', 'stare_pris', 'panta_(tail_concerto)',
	'russel_ryebread', 'princess_terria', 'cyan_garland', 'fool_(tail_concerto)', 'red_savarin',
	'chocolat_gelato', 'elh_melizee', 'beluga_damiens', 'quebec(solatorobo)', 'flo_financier',
	'bruno_dondurma', 'opera_kranz', 'gren_sacher', 'calua_napage', 'merveille_million',
	'nero_(solatorobo)', 'blanck_(solatorobo)', 'carmine_(solatorobo)', 'rose_(solatorobo)', 'malt_marzipan',
	'mei_marzipan', 'hanna_fondant', 'boron_brioche', 'kyle_bavarois', 'socks_million',
	'chick_montblanc', 'hack_montblanc', 'sheena_falafel', 'jin_macchiato', 'wappa_charlotte',
	'britz_strudel', 'muscat_(fuga)', 'shvein_hax', 'flam_kish', 'doktor_blutwurst']
	if meg != "":
		for x in range(len(char)):
			match = re.search(char[x], meg, flags=re.IGNORECASE)
			if match is not None:
				base_tags.append(chartag[x])
				break
			else:
				base_tags.append(random.choice(tuple(chartag)))
	else:
		base_tags.append(random.choice(tuple(chartag)))
	return base_tags

@Client.on_message(filters.command("char", "!"))
def pdg(client, message):
	leniency = 0
	tags[:] = []
	base_tags = ['little_tail_bronx', 'rating:safe']
	meg = " ".join(message.text.split()[1:])
	match_set = ['s', 'f', 't']
	tag_set = ['solatorobo', 'fuga:_melodies_of_steel', 'tail_concerto']
	if meg != "":
		for x in range(len(match_set)):
			match = re.search(match_set[x], meg, flags=re.IGNORECASE)
			if match is not None:				
				base_tags.append(tag_set[x])
				break
			else:
				base_tags = [random.choice(tuple(tag_set))]
	else:
		base_tags = [random.choice(tuple(tag_set))]
	pdgmatch(base_tags, meg)
	pic2(client, message, base_tags, tags, leniency)

#fine pic

@Client.on_message(filters.command("calc", "!"))
def calc(client, message):
	gesu = " ".join(message.text.split()[1:])
	#message.reply_text("<code>" + str(simple_eval(gesu)) + "</code>")
	message.reply_text(str(simple_eval(gesu)))

@Client.on_message(filters.command("no", "!"))
def no(client, message):
	pareo = [
		'puttane', 'bitches', 'figura materna',
		'figura paterna', 'women', 'ladies',
		'wenches', 'damsels', 'tette', 'poppe',
		'badonkadonks', 'zoccole', 'palle', 'zinne',
		'minne', 'chiappette', 'goth gf', 'femboy',
		'estrogeni', 'mizore'
	]
	#message.reply_text("<code>no " + random.choice(pareo) + "?</code>")
	message.reply_text(random.choice(pareo))

@Client.on_message(filters.command("sto", "!"))
def stocazzo(client, message):
	#message.reply_text("<code>cazzo</code>")
	message.reply_text("cazzo")

@Client.on_message(filters.command("mela", "!"))
def mela(client, message):
	#message.reply_text("<code>suchi</code>")
	message.reply_text("suchi")

@Client.on_message((filters.user(oliwia)) & filters.command("horny", "!"))
def horny(client, message):
	switchino(message)

@Client.on_message(filters.command("status", "!"))
def status(client, message):
	if consfw == True:
		h_stat = "Horny attivo"
	else:
		h_stat = "Horny non attivo"
	#message.reply_text("<code>" + h_stat + "</code>")
	message.reply_text(h_stat)

@Client.on_message(filters.command("misura", "!"))
def pisnelo(client, message):
	boh = ""	
	ah = randint(0, 30)
	for i in range(30):
		if i == ah:
			break
		else:
			boh += ":"
	#message.reply_text("<code>Ce l'hai lungo " + str(ah) + " cm\n8" + boh + "D</code>")
	message.reply_text("Ce l'hai lungo " + str(ah) + " cm\n8" + boh + "D")
	
@Client.on_message(filters.command("misure", "!"))
def popipopi(client, message):
	boh = ""
	momentino = [
		"Sei più piatta della Pianura Padana", "Hai una prima, belle tettine",
		"Hai una seconda, belle poppe", "Hai una terza, jamm bell",
		"Hai una quarta, dei furgoncioni",
		"Hai una quinta, delle mozzarellone, zizzone di Battipaglia",
		"E la madonna c'hai dei meloni sul petto, hai una sesta",
		"Hai una settima, due bombe in pratica",
		"E la madonna e che sei la Parmalat? Hai un'ottava"
	]
	ah = randint(0, 9)
	for i in range(9):
		if i == ah:
			break
		else:
			boh += " "
	#message.reply_text("(" + boh + "." + boh + ")(" + boh + "." + boh +")\n<code>" + momentino[ah] + "</code>")
	message.reply_text("(" + boh + "." + boh + ")(" + boh + "." + boh + "\n" + momentino[ah])

@Client.on_message(filters.command("porco", "!"))
def diocan(client, message):
	nome = [
		'dio', 'gesù', 'giuseppe',
		'giuda', 'cristo',
		'signore', 'allah', 'buddha'
	]
	bestemmiun = [
		'interrato', 'impalato', 'sgarrupato',
		'stuprato', 'gesuita',
		'incarcerato', 'inculato', ""
	]
	eun = len(nome)
	eddoje = len(bestemmiun)
	randr = randint(0, eun)
	randc = randint(0, eddoje)
	#maronn = "<code>porco " + nome[randr] + " " + bestemmiun[randc] + "</code>"
	maronn = "porco " + nome[randr] + " " + bestemmiun[randc]
	message.reply_text(maronn)

@Client.on_message(filters.command("poppe", "!"))
def tettazze(client, message):
	#message.reply_text("<code>ciao puttana, belle TETTE hahahahahha latte latttte latte baby ho tanta sete mamma hahahahaha stupida puttana dammi quelle mammellone zoccola haha tette tetta tettazze dammele io dare morsone alle tue tettone hahah popi popi popi sesso troia mammina popi popi latte baby ne voglio di più succhia succhia popi popi toc toc su quelle belle tettone lattose heee hee heeee haha aaaaaaaaa nessuno fermerà il furgoncino del latte haha poooo poooo ciuf ciuf tutti a bordo sul treno delle tettoneeee eeee wooooo poooo pooooo</code>")
	message.reply_text("ciao puttana, belle TETTE hahahahahha latte latttte latte baby ho tanta sete mamma hahahahaha stupida puttana dammi quelle mammellone zoccola haha tette tetta tettazze dammele io dare morsone alle tue tettone hahah popi popi popi sesso troia mammina popi popi latte baby ne voglio di più succhia succhia popi popi toc toc su quelle belle tettone lattose heee hee heeee haha aaaaaaaaa nessuno fermerà il furgoncino del latte haha poooo poooo ciuf ciuf tutti a bordo sul treno delle tettoneeee eeee wooooo poooo pooooo")

@Client.on_message(filters.command("cerca|search", "!"))
def cerca(client, message):
	meg = " ".join(message.text.split()[1:])
	message.reply_text("t.me/addstickers/" + meg)
	
@Client.on_message(filters.command("ext", "!"))
def pinnino(client, message):
	if message.reply_to_message.text != "":
		pinurl = message.reply_to_message.text
	else:
		pinurl = " ".join(message.text.split()[1:])
	pinreq = requests.get(pinurl)
	pinres = re.search("https://i\\.pinimg\\.com.*(jpg|gif|png)\"\\s", pinreq.text, flags=re.IGNORECASE)
	if pinres is not None:
		link_temp = pinres.group(0)
		pinlink = re.sub("\".*$", "", link_temp)
	else: 
		pinlink = "API timeout"
	message.reply_text(pinlink)

@Client.on_message(filters.command("fix", "!"))
def fixino(client, message):
	if message.reply_to_message.text != "":
		link = message.reply_to_message.text
	else:
		link = " ".join(message.text.split()[1:])
	twres = re.search("twitter", link, flags=re.IGNORECASE)
	inres = re.search("instagram", link, flags=re.IGNORECASE)
	if twres is not None:
		re.sub("twitter", "fxtwitter", link)
	elif inres is not None:
		re.sub("instagram", "ddinstagram", link)
	else:
		link = "Invalid link"
	message.reply_text(link)

@Client.on_message(filters.command("nome", "!"))
def nomino(client, message):
	diocristo = ['A-L', 'M-Z']
	diocanoide = random.choice(tuple(diocristo))
	url = "https://it.wikipedia.org/w/index.php?title=Prenomi_italiani_(" + diocanoide + ")&action=raw"
	r = requests.get(url)
	wt = wtp.parse(r.text)
	rand_tab = random.choice(wt.tables).data()
	rand_row = list(filter(None, random.choice(rand_tab)))
	# tabella
	rand_tab = random.choice(wt.tables).data()
	# riga
	rand_row = list(filter(None, random.choice(rand_tab)))
	# colonna
	rand_nam = re.sub(".*\\||\\]\\]", "", re.sub("\\[\\[#.*", "", re.sub("^\\[\\[", "", random.choice(rand_row))))
	#message.reply_text("<code>" + remove_markup(rand_nam, replace_wikilinks=False) + "</code>")
	message.reply_text(remove_markup(rand_nam, replace_wikilinks=False))

#Funzioni
	

def check_kris(message):
	global consfw
	chat_id = message.chat.id
	if chat_id == o_k_dm or chat_id == o_k_gr: isKris = True
	else: isKris = False
	consfw = not isKris
	return isKris

def switchino(message):
	global consfw
	inter = " ".join(message.text.split()[1:])
	if inter == "on":
		#message.reply_text("<code>Horny attivato</code>")		
		message.reply_text("Horny attivato")		
		consfw = True
	elif inter == "off":
		#message.reply_text("<code>Horny disattivato</code>")
		message.reply_text("Horny disattivato")
		consfw = False

def remover(base_tags: list, tags: list, limit):
	total = len(base_tags) + len(tags)
	#whitelist = ['rating:general', 'rating:safe', 'amy_rose', 'sticks_the_badger', 'sticks_the_jungle_badger', 'miles_prower', 'tails_(sonic)']
	#print("Base tags: " + str(len(base_tags)))
	#print("Tags: " + str(len(tags)))
	while total > limit:
		if not tags:
			copy = base_tags[2:-2]
			random.shuffle(copy)
			copy.pop()
			base_tags[2:-2] = copy
			total-=1
		else:
			tags.remove(random.choice(tags))
			total-=1
	tags.extend(base_tags)
	base_tags[:] = []
	return tags


def sceltina(char, match_set):
	temp = match_set.split(', ')
	matching = False
	for i in range(len(temp)):
		if char == temp[i]:
			index = i
			matching = True
			break
	if matching == False: index = randint(0, len(temp)-1)
	return index

def pic(client, message, base_tags: list, tags: list, leniency, should_have_m):
	(spoiler, l_iter, base_tags) = linkino(message, consfw, leniency, base_tags, True, should_have_m)
	tags.extend(no_video)
	query = remover(base_tags, tags, 100)
	#print(query)
	#print(len(query))
	isKris = check_kris(message)
	i = 0
	failsafe = 0
	while i < l_iter and failsafe < 100:
		try:
			req = json.loads(requests.get(gelbooru_api + '+'.join(query)).text)
			try:
				file_id = req['post'][0]['file_url']
				isSame = checkino_link(file_id, isKris)
				if not isSame and file_id != "":
					i+=1
					client.send_photo(
						chat_id=message.chat.id,
						photo=file_id,
						caption=file_id,
						has_spoiler=spoiler)
				else:
					failsafe+=1
			except WebpageCurlFailed:
				try:
					g_id = str(req['post'][0]['id'])
				except KeyError:
					failsafe+=1
				else:
					file_id = str(req['post'][0]['file_url'])
					isSame = checkino_link(file_id, isKris)
					if not isSame: 
						message.reply_text('https://gelbooru.com/index.php?page=post&s=view&id=' + g_id)
					else:
						failsafe+=1
		except KeyError:
			failsafe+=1
	tags[:] = []

def pic2(client, message, base_tags: list, tags: list, leniency):
	(spoiler, l_iter, base_tags) = linkino(message, consfw, leniency, base_tags, False, False)
	tags.extend(no_video)
	query = remover(base_tags, tags, 40)
	#print(query)
	#print(len(query))
	isKris = check_kris(message)
	url = e621_api + '+'.join(query)
	headers = {'user-agent' : 'olibot'}
	i = 0
	failsafe = 0
	while i < l_iter and failsafe < 100:
		try:
			req = json.loads(requests.get(url, headers=headers).text)
			file_id = req['posts'][0]['file']['url']
			isSame = checkino_link(file_id, isKris)
			try:
				if not isSame and file_id != "":
					i+=1
					client.send_photo(
						chat_id=message.chat.id,
						photo=file_id,
						caption=file_id,
						has_spoiler=spoiler)
					failsafe = 0
				else:
					failsafe+=1
			except WebpageCurlFailed:
				if not isSame:
					e_id = str(req['posts'][0]['id'])
					message.reply_text("https://e621.net/posts/" + e_id)
					failsafe = 0
				else:
					failsafe+=1
		except KeyError:
			failsafe+=1
	tags[:] = []

def linkino(message, consfw, leniency, base_tags:list, isGelbooru, should_have_m):
	if isGelbooru:
		sesso = random.choices(population=['rating:general', 'rating:sensitive'], weights=succinto(leniency))
	else:
		sesso = ['rating:safe']
	msg = " ".join(message.text.split()[1:])
	horny = "nsfw"
	spl = "spl"
	spoiler = False
	max = 10
	if horny in msg and consfw == True:
		leniency = 0
		sesso = random.choices(population=['rating:questionable', 'rating:explicit'], weights=succinto(leniency))
		if not should_have_m:
			base_tags.extend(schifo)
		else:
			base_tags.extend(schifo_w_m)
	if spl in msg: spoiler = True
	chunks = [int(s) for s in msg.split() if s.isdigit()]
	if chunks:
		l_iter = chunks[0]
		if l_iter >= max: l_iter = max
	else: l_iter = 1
	base_tags.insert(0, sesso[0])
	return (spoiler, l_iter, base_tags)

def checkino_link(file_id, isKris):
	blacklist = open('blacklist.txt', 'r')
	blacklist_k = open('blacklist_k.txt', 'r')
	blacklist_str = blacklist.readlines()
	blacklist_k_str = blacklist_k.readlines()
	if isKris:
		for x in blacklist_k_str:
			if file_id in x:
				blacklist.close()
				blacklist_k.close()
				return True
	else:
		for x in blacklist_str:
			if file_id in x:
				blacklist.close()
				blacklist_k.close()
				return True
	blacklist.close()
	blacklist_k.close()
	return False

def diocanajadedio(message):
	try:
		pattern = 'https://'
		spec = 'post'
		spec2 = '?id='
		error_stat = ''
		try:
			msg = message.reply_to_message.caption
			if pattern in msg:
				link = msg
		except (TypeError, AttributeError):
			error_stat = 'No caption'
			try:
				msg2 = message.reply_to_message.text
				if spec in msg2:
					link = msg2
			except (TypeError, AttributeError):
				link = ''
				error_stat = 'No reply'
			else:
				try:
					if spec2 in msg2:
						templ = msg2.split(spec2)
						url = gelbooru_api + 'id:' + templ[-1]
						req = json.loads(requests.get(url).text)
						link = req['post'][0]['file_url']
					else:
						templ = msg2.split(spec + 's/')
						url = "https://e621.net/posts.json?tags=id:" + templ[-1]
						headers = {'user-agent' : 'olibot'}
						req = json.loads(requests.get(url, headers=headers).text)
						link = req['posts'][0]['file']['url']
					error_stat = ''
				except KeyError:
					link = ''
					error_stat = 'No matching post'
	except (TypeError, AttributeError):
		link = ''
		error_stat = 'No reply'
	return (link, error_stat)


def succinto(leniency):
	match leniency:
		case 2: leniency = [0.1, 0.9]
		case 1: leniency = [0.7, 0.3]
		case 0: leniency = [0.5, 0.5]
		case -1: leniency = [1.0, 0.0]
	return leniency
