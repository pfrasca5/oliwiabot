#!/usr/bin/python3
from pyrogram import Client, filters, enums
import json 

with open("ids.json", "r") as f:
	data = json.loads(f.read())
	t_id = data['api_keys']['telegram_api_id']
	t_hash = data['api_keys']['telegram_api_hash']

app = Client(
	name = "my_account",
	api_id = t_id,
	api_hash = t_hash,
	plugins = dict(root="plugins"),
	parse_mode = enums.ParseMode.DISABLED
)

app.run() # start() and idle()
